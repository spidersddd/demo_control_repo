# @summary
#   This profile is to install SQL server and setup some site specific things.
#
# @note
#   'sa' admin login is disabled, although the password is set with this profile
#
# #param sql_iso_to_mount 
# # Location Path to SQL install media
#
# @param sa_password 
#   System Admin Password for SQL
#
# @param temp_db_location
#   Path to store temp dbs
#
# @param sql_source
#   Location of source files   
#
# @param sql_version_fact 
#   Set sql_version_fact to provided string
#
# @param use_sql_as_security_mode 
#   Set SQL security mode to 'SQL'
#
# @param sql_feature_array 
#   Array of additional SQL features
#
# @param sql_collation 
#   Set SQL_collation, Options: 'SQL_Latin1_General_CP1_CI_AS', 'SQL_Latin1_General_CP850_BIN2'.
#
class profile::app::sql::server (
# Commented out due to mount not working
#  Stdlib::Absolutepath $sql_iso_to_mount,
  String[1] $sa_password,
  Stdlib::Absolutepath $temp_db_location                                               = 'D:\\TempDB',
  Stdlib::Absolutepath $sql_source                                                     = 'K:\\',
  String[1] $sql_version_fact                                                          = 'SQL_2017',
  Boolean $use_sql_as_security_mode                                                    = false,
  Array $sql_feature_array                                                             = [ 'Conn', 'BC', 'SDK' ],
  Enum['SQL_Latin1_General_CP1_CI_AS', 'SQL_Latin1_General_CP850_BIN2'] $sql_collation = 'SQL_Latin1_General_CP1_CI_AS',
) {
  # resources
  file { $temp_db_location:
    ensure => directory,
  }

#  Non functional method to find and mount specified iso based on SQL version
#  if $facts['sqlserver_instances'][$sql_version_fact].empty and $facts['sqlserver_features'][$sql_version_fact] !=  $sql_feature_array {
#    class {'profile::tools::map_install_storage':
#      iso_to_mount => $sql_iso_to_mount,
#      before       => [ Sqlserver_instance['MSSQLSERVER'],Sqlserver_features['Generic Features'] ],
#    }
#  }

  if $use_sql_as_security_mode {
    sqlserver_instance { 'MSSQLSERVER':
      source                => $sql_source,
      features              => ['SQLEngine','FullText'],
      security_mode         => 'SQL',
      sa_pwd                => $sa_password,
      sql_svc_account       => 'SYSTEM',
      install_switches      => {
        'SQLTEMPDBDIR' => $temp_db_location,
        'SQLCOLLATION' => $sql_collation,
      },
      agt_svc_account       => 'SYSTEM',
      sql_sysadmin_accounts => 'BUILTIN\Administrators',
      require               => File[$temp_db_location],
    }
  } else {
    sqlserver_instance { 'MSSQLSERVER':
      source                => $sql_source,
      features              => ['SQLEngine','FullText'],
      sql_svc_account       => 'SYSTEM',
      install_switches      => {
        'SQLTEMPDBDIR' => $temp_db_location,
        'SQLCOLLATION' => $sql_collation,
      },
      agt_svc_account       => 'SYSTEM',
      sql_sysadmin_accounts => 'BUILTIN\Administrators',
      require               => File[$temp_db_location],
    }
  }

  sqlserver_features { 'Generic Features':
    source   => $sql_source,
    features => $sql_feature_array,
    require  => Sqlserver_instance['MSSQLSERVER'],
  }

  # Resource to connect to the DB instance
  sqlserver::config { 'MSSQLSERVER':
    admin_login_type => 'WINDOWS_LOGIN'
  }

  sqlserver::login {'sa':
    instance => 'MSSQLSERVER',
    disabled => true,
  }

  reboot { 'reboot after sql installation change':
      subscribe => [Sqlserver_instance['MSSQLSERVER'],Sqlserver_features['Generic Features']],
  }
}
