# @summary
#   This profile uses the icinga-icinga2 module to configure and install 
#   Ininga monitoring service
#
# @param manage_repo 
#   This identifies if ininga module should manage the package repository of ininga packages.
#
# @example Basic usage
#   include profile::app::icinga::server
class profile::app::icinga::server (
  Boolean $manage_repo = false,
) {
  class { '::icinga2':
    manage_repo => $manage_repo,
  }
}
