# @summary
#   This is an example profile to install MYSql using puppetlabs-mysql.
#
# @note
#   It will set root password for mysql set bindings and create databases
#   if specified.
#
# @example Set for PHP services 
#   class { 'profile::app::mysql::server':
#     mysql_bindings => [ 'php' ],
#   }
#   
# @param root_password 
#   root password for configuration of mysql
#
# @param mysql_bindings 
#   Bindings to set within mysql_bindings
#
# @param dbs
#   What dbs to create
#
class profile::app::mysql::server (
  # root_password is set in hiera data based on role and env_n_role
  #  Please delete files in "<control-repo/data/**/*.yaml"
  #Variant[String, Sensitive[String]] $root_password,
  String $root_password,
  Array[String] $mysql_bindings = [ 'php' ],
  Hash $dbs = {},
) {
  #This will verify the pp_preshared key exists and assign it to a local variable
  if ! $trusted['extensions']['pp_preshared_key'] {
    fail("ERROR: trusted.extensions.pp_preshared_key must be set to use ${title}.")
  } else {
    $tag_for_exported_mysql_db = $trusted['extensions']['pp_preshared_key']
  }

  #This will do a lookup to create one large hash from the hiera data
  $lookup_settings = lookup( { 'name' => 'profile::app::mysql::server::settings',
                                'merge' => {
                                  'strategy' => 'deep',
                                  'knockout_prefix' => '--',
                                },
  } )
  assert_type(Hash[String, Any], $lookup_settings)

  $network_option_hash = {
    'override_options' => {
      'mysqld' => {
        'bind-address' => $facts['networking']['ip'],
      }
    }
  }
  $merged_settings = deep_merge($network_option_hash, $lookup_settings)

  assert_type(String, $tag_for_exported_mysql_db)

  $secure_root_pass = $root_password

  class {  'mysql::server':
    root_password => $secure_root_pass,
    *             => $merged_settings,
  }
  contain mysql::server

  $mysql_bindings.each | String $binding | {
    contain "mysql::bindings::${binding}"
  }

  Mysql::Db <<| tag == $tag_for_exported_mysql_db |>>

}
