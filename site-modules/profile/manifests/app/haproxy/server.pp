# @summary
#   A profile to be used in a application type profile.
#
# @note
#   This profile is to create a haproxy server for a particular role services
#   and should have unique role identified for each haproxy servers
#
# @example Basic usage with hiera for listen_service
#   class { demo_control_repo::profile::app::haproxy::server:
#     calling_role => 'fastb_prod',
#   }
#
# @example Calling with listen_service specified 
#   class { demo_control_repo::profile::app::haproxy::server:
#     calling_role => 'fastb_prod',
#     listen_service => { 'web00' => { 'ports' => '80', 'mode' => 'http', }}
#   }
#
# @param listen_service
#   service name port and type for haproxy
#
# @param calling_role
#   This is the identifier of what service role this haproxy will be using this balancer.
#   This is used to lookup the listening service info in hiera
#   lookup("profile::app::haproxy::server::${calling_role}::${listen_service}"
#
class profile::app::haproxy::server (
  String $listen_service,
  String $calling_role,
) {
  include ::haproxy
  $deep_listen_services =  lookup("profile::app::haproxy::server::${calling_role}::${listen_service}", {merge => 'deep'})
  assert_type(Hash, $deep_listen_services)
  $deep_listen_services.each | String $service_name, Hash $params | {
    haproxy::listen { $service_name:
      collect_exported => true,
      ipaddress        => $facts['ipaddress'],
      *                => $params,
    }
  }
}
