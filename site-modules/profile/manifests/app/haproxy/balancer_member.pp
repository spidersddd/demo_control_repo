# @summary
#   This is a balancermember
#
# @param service_name
#   This is the title of the resource
#
# @param port 
#   What the port the service to be balanced will be running on
#
# @param options
#   What options should be passed to the haproxy::balancermember options param, defalut 'check'.
#
define profile::app::haproxy::balancer_member (
  String $service_name = $title,
  Variant[Array[Numeric],Numeric] $port = 80,
  String $options = 'check',
) {
  if ! $trusted['extensions']['pp_preshared_key'] {
    fail("ERROR: trusted.extensions.pp_preshared_key must be set to use ${title}.")
  }

  @@haproxy::balancermember { "${trusted}['extensions']['pp_preshared_key']-${facts['fqdn']}":
    listening_service => $trusted['extensions']['pp_preshared_key'],
    server_names      => $facts['fqdn'],
    ipaddresses       => $facts['ipaddress'],
    options           => $options,
    ports             => $port,
  }
}
