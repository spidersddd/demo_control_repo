# This is a profile for the fastb balancer
class profile::app::fastb::balancer {
  #This will verify the pp_preshared key exists and assign it to a local variable
  if ! $trusted['extensions']['pp_preshared_key'] {
    fail("ERROR: trusted.extensions.pp_preshared_key must be set to use ${title}.")
  }
  #This will verify the pp_role key exists and assign it to a local variable
  if ! $trusted['extensions']['pp_role'] {
    fail("ERROR: trusted.extensions.pp_role must be set to use ${title}.")
  }

  class { 'profile::app::haproxy::server':
    calling_role   => $trusted['extensions']['pp_role'],
    listen_service => $trusted['extensions']['pp_preshared_key'],
  }
}

