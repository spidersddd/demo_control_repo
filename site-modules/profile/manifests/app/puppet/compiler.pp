# @summary
#   This profile should be used on compiler hosts for Puppet Enterprise.
#   This profile has a chicken and egg complex.
#
# @note
#   The content of the keys cannot reside in hiera-eyaml until Primary Master has been configured.
#   These keys should be created and added once the Puppet Install is up and running.
#   The process for creating these keys are:
#     https://puppet.com/docs/pe/2019.2/code_mgr_config.html
#     https://github.com/voxpupuli/hiera-eyaml
#
# @param ssh_private_key_content 
#   Content of the ssh private key for Code Manager to use ssh protocol
#
# @param ssh_public_key_content 
#   Content of the ssh public key for git service usage
#
# @param eyaml_private_key_content 
#   EYAML private key to decrypt encrypted content in hiera data.
#
# @param eyaml_public_key_content 
#   EYAML public key to encrypt content in hiera data. To be used by developers.
#
class profile::app::puppet::compiler (
  String $ssh_private_key_content,
  String $ssh_public_key_content,
  String $eyaml_private_key_content,
  String $eyaml_public_key_content,
) {

  file { [ '/etc/puppetlabs/puppet/eyaml', '/etc/puppetlabs/puppetserver/ssh/' ]:
    ensure => directory,
    owner  => 'pe-puppet',
    group  => 'pe-puppet',
    mode   => '0750',
  }

  file { '/etc/puppetlabs/puppetserver/ssh/id-control_repo.rsa':
    ensure  => file,
    owner   => 'pe-puppet',
    group   => 'pe-puppet',
    content => $ssh_private_key_content,
  }

  file { '/etc/puppetlabs/puppetserver/ssh/id-control_repo.rsa.pub':
    ensure  => file,
    owner   => 'pe-puppet',
    group   => 'pe-puppet',
    content => $ssh_public_key_content,
  }

  file { '/etc/puppetlabs/puppet/eyaml/private_key.pkcs7.pem':
    ensure  => file,
    owner   => 'pe-puppet',
    group   => 'pe-puppet',
    mode    => '0600',
    content => $eyaml_private_key_content,
  }

  file { '/etc/puppetlabs/puppet/eyaml/public_key.pkcs7.pem':
    ensure  => file,
    owner   => 'pe-puppet',
    group   => 'pe-puppet',
    mode    => '0644',
    content => $eyaml_public_key_content,
  }
}
