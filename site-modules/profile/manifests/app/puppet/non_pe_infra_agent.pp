# @summary
#   class to help manage server settings for puppet.conf
#
# @param puppet_server
#   Puppet Service Name (fqdn of master, or dns alias to compilers)
#
# @param path_to_puppet_conf_dir 
#   Path to used to find puppet.conf file.
#
class profile::app::puppet::non_pe_infra_agent (
  String                         $puppet_server = 'puppet.exampledomain.com',
  Stdlib::Absolutepath $path_to_puppet_conf_dir = '/etc/puppetlabs/puppet',
) {
  ini_setting { 'puppet server setting':
    ensure  => present,
    path    => "${path_to_puppet_conf_dir}/puppet.conf",
    section => 'main',
    setting => 'server',
    value   => $puppet_server,
  }
}
