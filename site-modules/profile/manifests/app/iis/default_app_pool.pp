# @summary
#   example class of default IIS app pool. This profile will setup a basic
#   default app_pool for IIS using puppetlabs-iis.
#
# @param site_name 
#   Name of the site to be assigned.
#
class profile::app::iis::default_app_pool (
  String $site_name = 'Default Web Site'
) {
  $iis_features = ['Web-WebServer','Web-Scripting-Tools']

  iis_feature { $iis_features:
    ensure => 'present',
  }

  # Delete the default website to prevent a port binding conflict.
  iis_site {'Default Web Site':
    ensure  => absent,
    require => Iis_feature['Web-WebServer'],
  }

  iis_site { 'minimal':
    ensure          => 'started',
    physicalpath    => 'c:\\inetpub\\minimal',
    applicationpool => 'DefaultAppPool',
    require         => [
      File['minimal'],
      Iis_site[$site_name]
    ],
  }

  file { 'minimal':
    ensure => 'directory',
    path   => 'c:\\inetpub\\minimal',
  }
}
