# @summary
#   This class is to check and see if required trusted extensions are set.
#
# @note
#   The list of required trusted extensions can be placed in hiera or can be set
#   when the class is called using params
#
# @example Call class with list of required extensions
#   class { '::profile::os::baseline::verify_trusted_data':
#     extensions_to_check => [ 'pp_product', 'pp_role',
#       'pp_service' ],
#   }
#
# @param extensions_to_check
#   List of extensions to verify are not set to `undef`
#
class profile::os::baseline::verify_trusted_data (
  Optional[Array] $extensions_to_check = undef,
) {
  if $extensions_to_check != undef {
    $extensions_to_check.each | $key | {
      $extension_value = $trusted['extensions'][$key]
      if $extension_value == undef {
        fail("Required trusted.extensions.${key} does not exists!\nExtension must be set!")
      }
    }
  }
}
