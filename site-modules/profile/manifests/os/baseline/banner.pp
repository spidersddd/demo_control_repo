# @summary
#   This class will setup MOTD for Windows and Linux hosts
#
# @param motd
#   Message to be displayed as Legal Notice or MOTD
#
# @param windows_motd_title
#   Windows Legal Notice title
#
# @example Call class with Message of the day and Windows notice title
#   class { '::profile::os::baseline::banner':
#     motd               => 'This host is managed by Puppet Enterprise',
#     windows_motd_title => 'Puppet Set Legal Notice',
#   }
class profile::os::baseline::banner (
  String $motd,
  String $windows_motd_title = 'Puppet Set message of the Day',
) {
  class { 'motd':
    content            => $motd,
    windows_motd_title => $windows_motd_title,
  }
}
