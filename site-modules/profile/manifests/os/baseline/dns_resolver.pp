# @summary
#   This profile abstracts away the configuration of DNS settings into
#   2 parameters, 'name_servers' and 'search_path'.
#
# @note
#   The params are passed from the baseline profile.
#
# @param name_servers
#   An array of DNS name servers to use.
#
# @param search_path
#   An array of domain suffixes to use in the DNS search path.
#
# @param win_resolv_interface
#   A network interface to use for dns lookups.
#
# @param win_addressfamily
#   A network address type IPv4 or IPv6.
#
class profile::os::baseline::dns_resolver (
  Array[String[1]] $name_servers,
  Array[String[1]] $search_path,
  Optional[String[1]] $win_resolv_interface = undef,
  Enum['IPv4', 'IPv6'] $win_addressfamily = 'IPv4',
) {

  case $facts['kernel'] {
    'Linux','SunOS': {

      # Use saz/resolv_conf Forge module
      class { 'resolv_conf':
        nameservers => $name_servers,
        searchpath  => $search_path,
      }

    }
    'windows': {

      # Use the puppetlabs/dsc module
      # Rather than set every interface, you could instead just set the primary
      # by changing the dsc_interfacealias param to $facts['networking']['primary']
      # and removing the each loop.
      #  $facts['networking']['interfaces'].keys.each |$interface| {
      #    dsc_xdnsserveraddress { "Configure-DNS-${interface}-interface":
      #      ensure             => present,
      #      dsc_address        => $name_servers,
      #      dsc_interfacealias => $interface,
      #      dsc_addressfamily  => 'IPv4',
      #    }
      #  }
      if $win_resolv_interface {
        # This will set resolv for identified $win_resolv_interface
        dsc_xdnsserveraddress { "Configure-DNS-${win_resolv_interface}-interface":
          ensure             => present,
          dsc_address        => $name_servers,
          dsc_interfacealias => $facts['networking'][$win_resolv_interface],
          dsc_addressfamily  => $win_addressfamily,
        }
      } else {
        # This will set resolve for primary interface
        dsc_dnsserveraddress { 'Configure-DNS-primary-interface':
          ensure             => present,
          dsc_address        => $name_servers,
          dsc_interfacealias => $facts['networking']['primary'],
          dsc_addressfamily  => $win_addressfamily,
        }
      }
      dsc_dnsclientglobalsetting { 'Configure-DNS-search-path':
        ensure               => present,
        dsc_suffixsearchlist => $search_path,
        dsc_issingleinstance => 'Yes'
      }

    }
    default: { fail("This profile does not support your OS type ${facts['kernel']}") }
  }

}
