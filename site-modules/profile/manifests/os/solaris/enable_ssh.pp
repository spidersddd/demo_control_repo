# @summary
#   This class enables SSH and enables/disables root login
#
# @param permit_root_login
#   Allow root login through ssh
#
class profile::os::solaris::enable_ssh (
  Enum['yes', 'no'] $permit_root_login = 'yes',
) {

  # Start up the service and enable it at boot time.
  service { 'svc:/network/ssh:default':
    ensure => running,
    enable => true,
  }

  # Manage whether root is allowed to login.  (Default: yes)
  file_line { 'permit root ssh':
    ensure => present,
    path   => '/etc/ssh/sshd_config',
    line   => "PermitRootLogin ${permit_root_login}",
    match  => '^PermitRootLogin ',
    notify => Service['svc:/network/ssh:default'],
  }

}
