# @summary
#   This is a class example for security enhancements
#
# @param pe_environment
#   If you are using this module in a Puppet Enterprise (PE) environment,
#   you have to set pe_environment = true Otherwise puppet will drop an error
#   (duplicate resource)!
#
class profile::os::linux::security (
  Boolean $pe_environment = true,
) {
  class { '::os_hardening':
    pe_environment => $pe_environment,
  }
}
